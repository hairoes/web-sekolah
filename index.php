<?php 
require 'module/header.php'; 
if (isset($_GET['page'])) :
	require 'module/pages.php';
?>
	
<?php  elseif (isset($_GET['info'])) : 
	require 'module/info.php';
?>
<?php elseif (isset($_GET['info-detail'])) : 
	require 'module/info-detail.php';
?>
<?php elseif (isset($_GET['buku-tamu'])) :
	require 'module/buku-tamu.php';
?>
<?php else : ?>
<!-- Banner -->
			<section id="banner">
				<div class="inner">
					<h2>SMK ISLAM AL-MUNIR</h2>
					<p>the islamic school of science</p>
					<ul class="actions">
						<li><a href="#one" class="button big alt">Learn More</a></li>
					</ul>
				</div>
			</section>

		<!-- One -->
			<section id="one" class="wrapper style1">
				<header class="major">
					<h2>FASILITAS</h2>
					<p>Kebutuhan Dunia Pendidikan</p>
				</header>
				<div class="container">
					<div class="row">
						<div class="4u">
							<section class="special box">
								<i class="icon fa-book major"></i>
								<h3><a href="page.php?hl=perpustakaan">perpustakaan</a></h3>
								<p>Eu non col commodo accumsan ante mi. Commodo consectetur sed mi adipiscing accumsan ac nunc tincidunt lobortis.</p>
							</section>
						</div>
						<div class="4u">
							<section class="special box">
								<i class="icon fa-male major"></i>
								<h3><a href="page.php?hl=akademik">akademik</a></h3>
								<p>Eu non col commodo accumsan ante mi. Commodo consectetur sed mi adipiscing accumsan ac nunc tincidunt lobortis.</p>
							</section>
						</div>
						<div class="4u">
							<section class="special box">
								<i class="icon fa-refresh major"></i>
								<h3><a href="page.php?hl=laboratorium">laboratorium</a></h3>
								<p>Eu non col commodo accumsan ante mi. Commodo consectetur sed mi adipiscing accumsan ac nunc tincidunt lobortis.</p>
							</section>
						</div>
					</div>
				</div>
			</section>
			
		<!-- Two -->
		<section id="two" class="wrapper style2">
				<header class="major">
					<h2>INFRASTRUKTUR</h2>
					<p>Kelengkapan & Kenyamanan Layanan</p>
				</header>
				<div class="container">
					<div class="row">
						
			
						<div class="6u">
							<section class="special">
								<a href="#" class="image home"><img src="images/ruang-belajar.jpg" alt="" /></a>
								<h3>RUANG BELAJAR</h3>
								<p>Kenyamanan Belajar Mengajar</p>
								<ul class="actions">
									<li><a href="page.php?hl=kepsek-profil" class="button small alt">selengkapnya</a></li>
								</ul>
							</section>
						</div>
						<div class="6u">
							<section class="special">
								<a href="#" class="image home"><img src="images/lapangan.jpg" alt="" /></a>
								<h3>SARANA OLAHRAGA</h3>
								<p>Kebugaran & Kesehatan</p>
								<ul class="actions">
									<li><a href="page.php?hl=kepsek-sambutan" class="button small alt">selengkapnya</a></li>
								</ul>
							</section>
						</div>
					</div>
				</div>
			</section>

		<!-- Three -->
			<section id="three" class="wrapper style1">
				<div class="container">
					<div class="row">
						<div class="8u">
							<section>
								<?php 
								$data  = mysqli_fetch_array($db->query("SELECT * FROM info ORDER BY info_tanggal DESC LIMIT 1"));
								echo "
									<h2><a href='?info-detail=".$data['info_id']."'>".$data['info_title']."</a></h2>
									<img class='image fit' src='images/".$data['info_image']."' />
									<p>".substr($data['info_content'], 0, 300)." ...</p>";
								?>
							</section>
						</div>
						<div class="4u">
							<section>
								<h3>INFO LAINNYA</h3>
								<ul class="alt">
									<?php 
										$rs = $db->query("SELECT * FROM info ORDER BY info_tanggal ASC limit 5");
										while($rw = $rs->fetch_array()){
											echo "<li>
												<a href='?info-detail=".$rw['info_id']."'>".$rw['info_title']."</a>
												</li>";
										}
									?>
								</ul>
							</section>
						</div>
					</div>
				</div>
			</section>	
<?php
endif;
require 'module/footer.php';
?>