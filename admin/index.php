
<?php
    session_start();
    if (isset($_SESSION['MODGOD'])){
        header('location: dashboard.php?module=main');
    } elseif (isset($_POST['submit'])) {
            $username	= $_POST['username'];
            $password	= md5($_POST['password']);
            
            include("include/mysql.php");
            $login_query	= $db->query("SELECT * FROM admin WHERE username='$username' AND password='$password'");
            $login_num_rows	= $login_query->num_rows;
            if ($login_num_rows > 0){
                $login_fetch_array			= $login_query->fetch_array();
                $_SESSION['username']		= $login_fetch_array['username'];
                $_SESSION['password']		= $login_fetch_array['password'];
				$_SESSION['name']			= $login_fetch_array['admin_name'];
				$_SESSION['id']				= $login_fetch_array['admin_id'];
				$_SESSION['MODGOD']			= md5(time());
                
                $db->close();
                header('location: dashboard.php?module=main');
            } else {
                $db->close();
                session_destroy();
                header("location: ./");
            }
        
    
    } else {
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Login Admin</title>

<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/styles.css" rel="stylesheet">
<!--[if lt IE 9]>
<script src="js/html5shiv.js"></script>
<script src="js/respond.min.js"></script>
<![endif]-->

</head>

<body>
	
	<div class="row">
		<div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">
			<div class="login-panel panel panel-default">
				<div class="panel-heading">Login Admin</div>
				<div class="panel-body">
					<form action="index.php" method="post">
						<fieldset>
							<div class="form-group">
								<input class="form-control" placeholder="Username" name="username" type="text" autofocus="">
							</div>
							<div class="form-group">
								<input class="form-control" placeholder="Password" name="password" type="password" value="">
							</div>
							<div class="col-xs-offset-10">
							<button type="submit" name="submit" class="btn btn-primary">Login</button>
							</div>
						</fieldset>
					</form>
				</div>
			</div>
		</div><!-- /.col-->
	</div><!-- /.row -->	
	
	
</body>

</html>
<?php
    }
?>